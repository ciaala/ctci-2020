//
// Created by Francesco Fiduccia on 08.07.20.
//
#include "lib/custom_set.hxx"
#include "gtest/gtest.h"
/*
class IntAddition : public ::testing::Test {

};
 */
using namespace ffi;
TEST(CustomSet, find_nothing_if_empty) {
  CustomSet custom_set1(0, true);
  EXPECT_EQ(-1, custom_set1.findLocationFirstTrue());
  CustomSet custom_set2(0, false);
  EXPECT_EQ(-1, custom_set2.findLocationFirstTrue());
}

TEST(CustomSet, find_first_if_built_as_true) {
  {
    CustomSet custom_set1(1, true);
    EXPECT_EQ(0, custom_set1.findLocationFirstTrue());
  }
  {
    CustomSet custom_set1(20, true);
    EXPECT_EQ(0, custom_set1.findLocationFirstTrue());
  }
}

TEST(CustomSet, find_none_if_built_as_false) {
  {
    CustomSet custom_set1(1, false);
    EXPECT_EQ(-1, custom_set1.findLocationFirstTrue());
  }
  {
    CustomSet custom_set1(20, false);
    EXPECT_EQ(-1, custom_set1.findLocationFirstTrue());
  }
}

TEST(CustomSet, find_none_when_default_is_false_and_extra_bits) {
  {
    CustomSet custom_set1(CustomSet::BINARY_GRANULARITY + 1, true);
    for (int i = 0; i < CustomSet::BINARY_GRANULARITY + 1; ++i) {
      custom_set1.set(i, false);
    }

    EXPECT_EQ(-1, custom_set1.findLocationFirstTrue());
  }
  {
    CustomSet custom_set1(CustomSet::BINARY_GRANULARITY + 1, false);
    for (int i = 0; i < CustomSet::BINARY_GRANULARITY + 1; ++i) {
      custom_set1.set(i, false);
    }

    EXPECT_EQ(-1, custom_set1.findLocationFirstTrue());
  }
}

TEST(CustomSet, less_than_one_granularity_works) {
  {
    CustomSet custom_set1(CustomSet::BINARY_GRANULARITY - 1, true);
    EXPECT_EQ(0, custom_set1.findLocationFirstTrue());
  }
  {
    CustomSet custom_set1(CustomSet::BINARY_GRANULARITY - 1, false);
    EXPECT_EQ(-1, custom_set1.findLocationFirstTrue());
  }
}
TEST(CustomSet, switching_bits_works) {

  CustomSet custom_set1(CustomSet::BINARY_GRANULARITY - 1, true);
  {
    int i = 0;
    while (custom_set1.countTrue() > 0) {
      custom_set1.set(i, false);
      i++;
    }
    EXPECT_EQ(CustomSet::BINARY_GRANULARITY - 1, i);
  }
  {
    // CustomSet custom_set1(CustomSet::BINARY_GRANULARITY - 1, true);
    int i = 0;
    while (custom_set1.countTrue() < CustomSet::BINARY_GRANULARITY-1) {
      custom_set1.set(i, true);
      i++;
    }
    EXPECT_EQ(CustomSet::BINARY_GRANULARITY - 1, i);
  }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}