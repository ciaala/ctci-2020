//
// Created by Francesco Fiduccia on 28.06.20.
//

#include <iostream>
#include "ex_04_04.hxx"

/**
 * Check Balanced
 *
 */

using namespace std;
typedef ffi::BinaryTreeNode<uint32_t> IntBinaryTree
;

namespace ffi::_04::_04 {

void run() {
  cout << "04_04" << endl;
  IntBinaryTree a(4);
  a.create_left(2)->left()
      ->create_right(3)
      ->create_left(1);
  a.create_right(6)->right()
      ->create_left(5)
      ->create_right(7);
  cout << a.value();
}
};