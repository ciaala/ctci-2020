//
// Created by Francesco Fiduccia on 03.07.20.
//

#include <array>
#include "ex_08_02.hxx"
#include <iostream>
#include <lib/grid_2_d.hxx>
#include <lib/console.hxx>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <stack>
#include <list>

namespace ffi::_08::_02 {
using namespace std;

enum GridCell {
  Empty = ' ',
  Rock = '#',
  Player = '@',
  Path = '~',
  Excluded = '0',
  Target = 'X'
};

void run() {
  EX_08_02 e;
  e.run();
}

struct Move {
  std::pair<unsigned, unsigned> position;
  bool hasMoveDown;
  bool hasMoveRight;

  explicit Move(unsigned i = 0, unsigned j = 0) :
      position(i, j),
      hasMoveDown(false),
      hasMoveRight(false) {
  }
};
template<unsigned gridSize>
void printGrid(
    const Grid2D<GridCell, gridSize> &grid,
    const pair<unsigned, unsigned> &player,
    const unordered_set<pair<unsigned, unsigned>, hash_pair> &avoidBlocks,
    const list<Move> &path) {

  ffi::Console::clear();
  cout << '(' << player.first << ',' << player.second << ')' << endl;
  function<GridCell(unsigned, unsigned, GridCell)> cellRenderer =
      [&player, &avoidBlocks, &path](unsigned i, unsigned j, GridCell cell) {
        if (i == gridSize - 1 && j == gridSize - 1) {
          return GridCell::Target;
        } else if (player.first == i && player.second == j) {
          return GridCell::Player;
        } else if (avoidBlocks.count(make_pair(i, j))) {
          return GridCell::Excluded;
        } else {
          bool found = path.crend() != find_if(path.crbegin(),
                                               path.crend(),
                                               [i, j](auto &o) {
                                                 return i == o.position.first && j == o.position.second;
                                               });
          return found ? GridCell::Path : cell;
        }
      };
  grid.print(cellRenderer, cout);
}

template<unsigned gridSize>
bool shouldMove(
    const unsigned int i,
    const unsigned int j,
    const ffi::Grid2D<GridCell, gridSize> &grid,
    const unordered_set<pair<unsigned, unsigned>, hash_pair> &avoid_blocks) {

  if (i < gridSize && j < gridSize) {
    if (avoid_blocks.count(make_pair(i, j)) == 0) {
      return grid.get(i, j) == GridCell::Empty;
    }
  }
  return false;
}

void EX_08_02::run() {

  const unsigned gridSize = 8u;
  Grid2D<GridCell, gridSize> grid(GridCell::Empty);
  grid.set(1,  1, GridCell::Rock);
  grid.set(0,  2, GridCell::Rock);
  grid.set(gridSize - 2, gridSize - 1, GridCell::Rock);
  grid.set(gridSize - 2, gridSize - 2, GridCell::Rock);
  unordered_set<pair<unsigned, unsigned>, hash_pair> avoid_blocks;
  list<Move> path = {};
  printGrid(grid, make_pair(0, 0), avoid_blocks, path);

  auto i = 0u;
  auto j = 0u;
  auto moves = gridSize * 5;
  auto endPosition = gridSize - 1;
  path.push_front(Move(i, j));
  while ((i != endPosition || j != endPosition) && moves) {
    printGrid(grid, make_pair(i, j), avoid_blocks, path);
    // move right

    if (j < endPosition && shouldMove(i, j + 1, grid, avoid_blocks)) {
      path.front().hasMoveRight = true;
      j++;
      path.push_front(Move(i, j));
    } else {
    //  path.front().hasMoveRight = false;
      // cannot move right
      if (i < endPosition && shouldMove(i + 1, j, grid, avoid_blocks)) {
      //  path.front().hasMoveDown = true;
        i++;
        path.push_front(Move(i, j));
      } else {
     //   path.front().hasMoveDown = false;
        avoid_blocks.emplace(path.front().position);
        path.pop_front();
        i = path.front().position.first;
        j = path.front().position.second;
      }
    }
    moves--;
  }
  printGrid(grid, make_pair(i, j), avoid_blocks, path);
}

}

