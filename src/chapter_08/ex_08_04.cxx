//
// Created by Francesco Fiduccia on 04.07.20.
//

#include "ex_08_04.hxx"
#include <iostream>
#include <unordered_set>
#include <lib/dice.hxx>
#include <array>
#include <list>
#include <stack>
#include <iomanip>
#include <sstream>
namespace ffi::_08::_04 {

void run() {
  try {
    EX_08_04<char> e;
    cout << endl << "=== 08_04 === " << e.name << endl;
    e.run();
  } catch (std::exception &e) {
    cout << "Error: '" << e.what() << "'" << endl;
  }
}
template<typename T>
ostream &operator<<(ostream &out, const vector<T> &flagSelected) {
  for_each(flagSelected.cbegin(), flagSelected.cend(), [&out](T e) { out << e; });
  return out;
}

template<typename T>
ostream &operator<<(ostream &out, const unordered_set<T> &flagSelected) {
  vector<T> elements(flagSelected.begin(), flagSelected.end());
  sort(elements.begin(), elements.end());
  for (int i = 0; i < elements.size(); i++) {
    out << ' ' << elements[i];
    //out << setw(3) << i << '@' << elements[i];
    //for_each(elements.begin(), elements.end(), [&out](T e) { out << e; });
  }
  return out;
}
template<typename T>
void EX_08_04<T>::run() {

  vector<T> elements{'a', 'b', 'c', 'd', 'e'};
/*
  Dice dice;
  for (int i = 1; i < elements.size(); ++i) {
    elements[i] = elements[i - 1] + dice.next(1, 3);
  }
  */
  list<unordered_set<T>> powerSet = this->makePowerSet(elements);
  cout << "----" << endl << "Result:" << endl;
  for (const unordered_set<T> &set : powerSet) {
    cout << set << endl;
  }
}

template<typename T>
list<unordered_set<T>> EX_08_04<T>::makePowerSet(const vector<T> &elements) const {
  unordered_set<char> baseSet;
  baseSet.insert(elements.cbegin(), elements.cend());
  const auto lastIndex = elements.size() - 1;

  vector<int32_t> flaggedIndexes;
  flaggedIndexes.reserve(elements.size());
  list<unordered_set<char>> result;

  auto i = 0;
  auto moves = 64;
  cout << "RUN ";
  printStatus(baseSet, flaggedIndexes, i, lastIndex);

  do {
    moves--;
    if (baseSet.size() == 1 || i > lastIndex) {
      cout << "pop " ;
      i = fsm_pop(elements, baseSet, flaggedIndexes);
    } else if (baseSet.size() > 1) {
      cout << "push" ;
      i = fsm_forward(elements, flaggedIndexes, result, baseSet, i);
    }
    printStatus(baseSet, flaggedIndexes, i, lastIndex);

  } while ((!flaggedIndexes.empty() || (i < (elements.size()))) && moves);
  cout << "end" << endl;
  cout << "----" << endl << "moves: " << moves << endl;
  return result;
}

template<typename T>
void EX_08_04<T>::printStatus(const unordered_set<char> &baseSet,
                              const vector<int32_t> &flaggedIndexes,
                              const int i,
                              const int elements_count) const {
  stringstream tempBaseSet;
  stringstream tempFlaggedIndexes;
  // stringstream temp;
  tempBaseSet << baseSet;
  tempFlaggedIndexes << flaggedIndexes;
  auto n = elements_count + 1;
  cout << setfill(' ') << "| " << i
       << " | " << setw(n) << tempFlaggedIndexes.str()
       << " | " << setw(2* n) << tempBaseSet.str()
       << " |" << endl;
}

template<typename T>
int EX_08_04<T>::fsm_pop(const vector<T> &elements,
                         unordered_set<char> &baseSet,
                         vector<int32_t> &flaggedIndexes) const {// POP

  auto index = flaggedIndexes.back();
  flaggedIndexes.pop_back();
  baseSet.insert(elements[index]);
/*
  if (index >= elements.size() - 1) {
    index = flaggedIndexes.back();
    flaggedIndexes.pop_back();
    baseSet.insert(elements[index]);

  }
  */
  return index + 1;
}

template<typename T>
int EX_08_04<T>::fsm_forward(const vector<T> &elements,
                             vector<int32_t> &flaggedIndexes,
                             list<unordered_set<char>> &result,
                             unordered_set<char> &baseSet,
                             const int &i) const {// FORWARD

  baseSet.erase(elements[i]);
  result.emplace_back(baseSet);
  // cout << setw(32) << setfill('+') << baseSet << endl;
  flaggedIndexes.push_back(i);

  int32_t nextTrue = i + 1;
  return nextTrue;
}


/*
int32_t firstTrueIndex(const vector<bool> &flagSelected,
                       int32_t startIndex) {
  while (startIndex < flagSelected.size() && !flagSelected[startIndex]) { startIndex++; };
  return startIndex < flagSelected.size() ? startIndex : -1;
}

void updateResult(const vector<int32_t> &elements,
                  const unordered_set<int32_t> &baseSet,
                  list<unordered_set<int32_t>> &result,
                  int j) {
  //unordered_set<int32_t> picked(baseSet);
  //picked.erase(elements[j]);
  result.push_back(reulst);
}
*/

}