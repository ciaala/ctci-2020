//
// Created by Francesco Fiduccia on 11.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_EX_08_09_HXX_
#define CTCI_2020_SRC_CHAPTER_08_EX_08_09_HXX_
#include <string>

#include <unordered_set>
#include <unordered_map>
#include <lib/dice.hxx>
#include <array>
#include <list>
#include <lib/custom_set.hxx>

namespace ffi::_08::_09 {
void run();
class EX_08_09 {
 public:
  string name = "Generate Parenthesis";

  void run();
  explicit EX_08_09(unsigned number);

 private:
  string result;
  unsigned number;
  unsigned resultLength;
  list<string> results;

  void generateNextStep(unordered_map<char, unsigned> &available);
};
}

#endif //CTCI_2020_SRC_CHAPTER_08_EX_08_09_HXX_
