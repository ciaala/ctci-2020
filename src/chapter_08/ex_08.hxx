//
// Created by Francesco Fiduccia on 01.07.20.
//

#ifndef GOOGLE_2020_SRC_CHAPTER_08_EX_08_HXX_
#define GOOGLE_2020_SRC_CHAPTER_08_EX_08_HXX_
#include "ex_08_01.hxx"
#include "ex_08_02.hxx"
#include "ex_08_03.hxx"
#include "ex_08_04.hxx"
#include "ex_08_05.hxx"
#include "ex_08_06.hxx"
#include "ex_08_07.hxx"
#include "ex_08_08.hxx"
#include "ex_08_09.hxx"
#include "ex_08_10.hxx"

#include <iostream>

namespace ffi::_08 {


using namespace std;

void run() {
  // ffi::_08::_01::run();
  // ffi::_08::_02::run();
  // ffi::_08::_03::run();
  // ffi::_08::_04::run();
  // ffi::_08::_05::run();
  // ffi::_08::_06::run();
  // ffi::_08::_07::run();
  //ffi::_08::_08::run();
  //ffi::_08::_09::run();
  ffi::_08::_10::run();

}

}
#endif //GOOGLE_2020_SRC_CHAPTER_08_EX_08_HXX_
