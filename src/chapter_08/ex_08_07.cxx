//
// Created by Francesco Fiduccia on 07.07.20.
//

#include "ex_08_07.hxx"
#include "./lib/stream_extra.hxx"
#include "./lib/benchmark.hxx"

namespace ffi::_08::_07 {

void run() {
  try {
    EX_08_07 e("abcdefghjkl");
    cout << endl << "=== 08_07 === " << e.name << endl;
    e.run();
  } catch (std::exception &e) {
    cout << "Error: '" << e.what() << "'" << endl;
  }
}
void EX_08_07::generatePreviousPermutation(unordered_set<char> &characters,
                                           vector<string> &results,
                                           string &partial) {
  if (characters.size() == 1) {
    results.push_back(partial + *characters.begin());
    return;
  }
  vector<char> elements(characters.begin(), characters.end());
  unsigned position = partial.length();
  partial.push_back(' ');
  for (int i = 0; i < elements.size(); i++) {
    char current = elements[i];
    partial[position] = current;
    characters.erase(current);
    generatePreviousPermutation(characters, results, partial);
    characters.insert(current);
  }
  partial.erase(position, 1);
}

void EX_08_07::generatePermutation(vector<string> &results,
                                   string &partial) {

  if (custom_set.countTrue() == 1) {
    auto location = custom_set.findLocationFirstTrue();
    results.push_back(partial + elements[location]);
    return;
  }

  unsigned position = partial.length();
  partial.push_back(' ');

  auto location = custom_set.findLocationFirstTrue();

  while (location != -1) {

    partial[position] = elements[location];

    custom_set.set(location, false);
    generatePermutation(results, partial);

    // characters.insert(current);
    auto nextLocation = custom_set.findLocationFirstTrue(location);
    custom_set.set(location, true);
    location = nextLocation;
  }
  partial.erase(position, 1);
}

void EX_08_07::run() {

  {
    vector<string> results;

    TimeBenchmark t(this->name);

    string partial;

    t.start();
    // ----
    generatePermutation(results, partial);
    t.finish();
    cout << t << endl;
    cout << results.size() << endl;
    if (results.size() < 100) {
      cout << results << endl;
    }
  }
  if (0) {
    vector<string> results;

    TimeBenchmark t(this->name + " old implementation");
    // --
    unordered_set<char> characters(elements.cbegin(), elements.cend());
    characters.reserve(2 * elements.size());
    // --
    string partial;
    t.start();
    generatePreviousPermutation(characters, results, partial);
    t.finish();
    cout << t << endl;
    cout << results.size() << endl;

  }
  // ----

  //cout << results << endl;
}

EX_08_07::EX_08_07(string text) :
    elements(text),
    custom_set(text.length(), true) {
}

}