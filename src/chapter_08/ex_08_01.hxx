//
// Created by Francesco Fiduccia on 01.07.20.
//

#ifndef GOOGLE_2020_SRC_CHAPTER_08_EX_08_01_HXX_
#define GOOGLE_2020_SRC_CHAPTER_08_EX_08_01_HXX_
namespace ffi::_08::_01 {

void run();

class EX_08_01 {
 public:
  void run();
  uint64_t countSteps(const uint32_t &stairs);
};
}

#endif //GOOGLE_2020_SRC_CHAPTER_08_EX_08_01_HXX_
