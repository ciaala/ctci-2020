//
// Created by Francesco Fiduccia on 07.07.20.
//

#include "ex_08_06.hxx"
#include "lib/stream_extra.hxx"
namespace ffi::_08::_06 {
void run() {
  try {
    EX_08_06 e;
    cout << endl << "=== 08_06 === " << e.name << endl;
    e.run(4);
  } catch (std::exception &e) {
    cout << "Error: '" << e.what() << "'" << endl;
  }
}

void EX_08_06::run(const unsigned int height) {
  array<vector<unsigned>, 3> towers;
  for (unsigned i = height; i > 0; --i) {
    towers[0].emplace_back(i);
  }
  printTowers(towers);
  this->moveTower(towers, height, 0, 1, 2);
  printTowers(towers);
}

void EX_08_06::moveTower(array<vector<unsigned>, 3> &towers,
                         const unsigned disks,
                         const unsigned source,
                         const unsigned target,
                         const unsigned support) {
  if (disks > 1) {
    moveTower(towers, disks - 1, source, support, target);
    printTowers(towers);

    unsigned disk = towers[source].back();
    towers[source].pop_back();
    towers[target].emplace_back(disk);

    printTowers(towers);
    moveTower(towers, disks - 1, support, target, source);
  } else {
    unsigned disk = towers[source].back();
    towers[source].pop_back();

    (towers[target]).emplace_back(disk);
  }
}
void EX_08_06::printTowers(const array<vector<unsigned>, 3> &towers) const {
  for (int j = 0; j < towers.size(); ++j) {
    cout << j << "] " << towers[j] << endl;
  }
  cout << setw(16) << setfill('*') << "" << endl;
}

}