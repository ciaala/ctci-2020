//
// Created by Francesco Fiduccia on 07.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_EX_08_05_HXX_
#define CTCI_2020_SRC_CHAPTER_08_EX_08_05_HXX_
#include <string>
#include <unordered_set>
#include <lib/dice.hxx>
#include <array>
#include <list>

namespace ffi::_08::_05 {
using namespace std;

void run();
class EX_08_05 {
 private:
  long execute(const long n, const long m);
 public:
  string name = "Multiplication - ";
  void run(const long n, const long m);

};
}

#endif //CTCI_2020_SRC_CHAPTER_08_EX_08_05_HXX_
