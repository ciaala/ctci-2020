//
// Created by Francesco Fiduccia on 03.07.20.
//

#ifndef GOOGLE_2020_SRC_CHAPTER_08_EX_08_02_HXX_
#define GOOGLE_2020_SRC_CHAPTER_08_EX_08_02_HXX_
#include <string>
#include <array>
#include <lib/grid_2_d.hxx>

namespace ffi::_08::_02 {
void run();
using namespace std;
class EX_08_02 {

 private:
  const bool shouldClearScreen = false;
  const std::string name = "Robot in a grid";
 public:
  void run();

};

}
#endif //GOOGLE_2020_SRC_CHAPTER_08_EX_08_02_HXX_
