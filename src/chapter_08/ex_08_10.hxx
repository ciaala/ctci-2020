//
// Created by Francesco Fiduccia on 11.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_EX_08_10_HXX_
#define CTCI_2020_SRC_CHAPTER_08_EX_08_10_HXX_
#include <string>

#include <unordered_set>
#include <unordered_map>
#include <lib/dice.hxx>
#include <array>
#include <list>
#include <lib/custom_set.hxx>

namespace ffi::_08::_10 {

void run();

typedef uint32_t Color;
typedef pair<unsigned, unsigned> Point;
typedef vector<vector<Color>> Image;

enum Direction {
  up = 0, down = 1, left = 2, right = 3,
};

typedef bitset<4> DirectionSet;

class EX_08_10 {
 public:
  const string name = "Image Fill at Point with Color";
  void run(Point p, Color color);
  explicit EX_08_10(unsigned size);

 private:
  Image image;
  list<Point> frontier;
  vector<vector<bool>> evaluated;

  Color originalColor;
  Color overrideColor;

  static const vector<pair<int, int>> directions;

  void evaluateCell(const Point &point,
                    const int dx,
                    const int dy
  );
};

#endif //CTCI_2020_SRC_CHAPTER_08_EX_08_10_HXX_
}