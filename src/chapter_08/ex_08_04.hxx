//
// Created by Francesco Fiduccia on 04.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_EX_08_04_HXX_
#define CTCI_2020_SRC_CHAPTER_08_EX_08_04_HXX_

#include <string>
#include <unordered_set>
#include <lib/dice.hxx>
#include <array>
#include <list>

namespace ffi::_08::_04 {
using namespace std;

void run();

template<typename T>
class EX_08_04 {

 private:
  int fsm_forward(const vector<T> &elements,
                  vector<int32_t> &flaggedIndexes,
                  list<unordered_set<char>> &result,
                  unordered_set<char> &baseSet,
                  const int &i) const;

  int fsm_pop(const vector<T> &elements, unordered_set<char> &baseSet, vector<int32_t> &flaggedIndexes) const;

  list<unordered_set<T>> makePowerSet(const vector<T> &elements) const;

 public:
  string name = "Power Set - all super Subset";
  void run();

  void printStatus(const unordered_set<char> &baseSet,
                   const vector<int32_t> &flaggedIndexes,
                   const int i,
                   const int elements_count) const;
};
}
#endif //CTCI_2020_SRC_CHAPTER_08_EX_08_04_HXX_
