//
// Created by Francesco Fiduccia on 11.07.20.
//

#include "ex_08_09.hxx"
#include "lib/stream_extra.hxx"

namespace ffi::_08::_09 {

void run() {
  for (int i = 1; i <26; i+= 3) {
    try {
      EX_08_09 e(i);
      cout << endl << "=== 08_09 === " << e.name << endl;
      e.run();
    } catch (std::exception &e) {
      cout << "Error: '" << e.what() << "'" << endl;
    }
  }
}
void EX_08_09::generateNextStep(unordered_map<char, unsigned> &available) {

  if (result.length() == resultLength) {
    results.push_back(result);
    return;
  }

  if (available['('] > 0) {
    available['(']--;
    available[')']++;
    result.push_back('(');
    generateNextStep(available);
    available['(']++;
    available[')']--;
    result.pop_back();

  }
  if (available[')'] > 0) {
    available[')']--;
    result.push_back(')');
    generateNextStep(available);
    available[')']++;
    result.pop_back();
  }
}

void EX_08_09::run() {
  unordered_map<char, unsigned> available;
  //unsigned totalCount = 4;
  available['('] = number;
  available[')'] = 0;
  generateNextStep(available);
  cout << "parenthesis: " << number << endl;
  cout << "solutions:   " << results.size() << endl;
  if (results.size() < 20) {
    cout << results << endl;
  }
}

EX_08_09::EX_08_09(unsigned number) :
    resultLength(2 * number),
    number(number) {
}
}