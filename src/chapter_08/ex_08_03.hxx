//
// Created by Francesco Fiduccia on 04.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_EX_08_03_HXX_
#define CTCI_2020_SRC_CHAPTER_08_EX_08_03_HXX_
#include <string>


namespace ffi::_08::_03 {
using namespace std;
void run();
class EX_08_03 {

 public:
  const string name = "Magic Index";
  void run();
};
}

#endif //CTCI_2020_SRC_CHAPTER_08_EX_08_03_HXX_
