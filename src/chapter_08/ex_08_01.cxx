//
// Created by Francesco Fiduccia on 01.07.20.
//

#include <iostream>
#include "ex_08_01.hxx"

using namespace std;
namespace ffi::_08::_01 {

void run() {
  try {
    EX_08_01 e;
    cout << "=== 08_01 ===" << endl;
    e.run();
  } catch (std::exception &e) {
    cout << "Error: '" << e.what() << "'" << endl;
  }
}

void EX_08_01::run() {
  for (int i = 0; i < 10; i++) {
    cout << "run " << i << " " <<  this->countSteps(i) << endl;
  }

}

uint64_t EX_08_01::countSteps(const uint32_t &stairs) {
  if (stairs == 0) {
    return 0;
  }
  uint64_t previous[3] = {0, 0, 1};

  uint32_t i = 0;
  while (i <= stairs) {
    uint64_t count = previous[0] + previous[1] + previous[2];

    previous[2] = previous[1];
    previous[1] = previous[0];
    previous[0] = count;
    i++;
  }

  return previous[0];
}

}