//
// Created by Francesco Fiduccia on 07.07.20.
//

#include <sstream>
#include "ex_08_08.hxx"
#include "./lib/stream_extra.hxx"
#include "./lib/benchmark.hxx"
#include "algorithm"
namespace ffi::_08::_08 {

void run() {
  try {
    EX_08_08 e("abcdefghjk");
    cout << endl << "=== 08_08 === " << e.name << endl;
    e.run();
  } catch (std::exception &e) {
    cout << "Error: '" << e.what() << "'" << endl;
  }
}

/**
 *
 * @param foundChar if the character is found
 * @return
 */
bool EX_08_08::consumeNextCharacter(unsigned &index, char &foundChar) {

  while (index <= maxCharacterIndex) {
    auto current = characters[index];
    if (characterCountMap[current] > 0) {
      characterCountMap[current]--;
      currentCharactersCount--;
      foundChar = current;
      index++;
      return true;
    }
    index++;
  }
  index++;
  return false;
}

void EX_08_08::pushSolution(const unsigned position) {
  char foundChar;
  unsigned index = 0;
  if (consumeNextCharacter(index, foundChar)) {
    partial[position] = foundChar;
    if (printStatus) {
      printStepStatus(index, foundChar);
      cout << " ++ " << endl;
    }
    results.push_back(partial);
    reinsertConsumedChar(foundChar);
    partial[position] = ' ';
  }
}

void EX_08_08::generatePermutation(const unsigned position) {
  unsigned index = 0;
  char foundChar;
  if (position == sourceStringLength - 1) {
    pushSolution(position);
    return;
  }

  consumeNextCharacter(index, foundChar);

  while ((index <= maxCharacterIndex + 1) && moves > 0) {
    auto current = foundChar;
    partial[position] = current;
    if (printStatus) {
      printStepStatus(index, current);
      cout << endl;
    }
    moves--;
    generatePermutation(position + 1);

    consumeNextCharacter(index, foundChar);
    reinsertConsumedChar(current);

  }
  partial[position] = ' ';
}
void EX_08_08::printStepStatus(unsigned int index, char foundChar) const {
  cout << "| " << setw(2) << moves << " | " << index << " | " << partial << " | " << foundChar << " |";
}

void EX_08_08::run() {
  {
    TimeBenchmark t(this->name);
    {
      t.start();
      generatePermutation();
      t.finish();
    }

    cout << t << endl;
    stringstream ss;
    ss.imbue(locale(""));
    ss << fixed << (INT_MAX - moves);
    cout << "moves: " << ss.str() << endl;
    cout << "results: #" << results.size() << endl;
    auto uniqueSet = unordered_set<string>(begin(results), end(results));

    if (!results.empty() && results.size() < 100) {
      cout << "results: " << results << endl;
      cout << "unique:  " << uniqueSet << endl;
    }

  }
}

EX_08_08::EX_08_08(const string &text, bool printStatus) : printStatus(printStatus) {
  moves = INT_MAX;
  std::for_each(
      begin(text), end(text),
      [this](char c) {
        this->characterCountMap[c] += 1;
      }
  );
  characters.reserve(characterCountMap.size());
  for_each(begin(characterCountMap),
           end(characterCountMap),
           [this](const pair<char, unsigned> &entry) {
             this->characters.push_back(entry.first);
           });
  currentCharactersCount = text.length();
  sourceStringLength = currentCharactersCount;
  maxCharacterIndex = characters.size() - 1;
  partial.resize(text.size(), ' ');
}

void EX_08_08::reinsertConsumedChar(char a_char) {
  characterCountMap[a_char] += 1;
  currentCharactersCount++;
}

}