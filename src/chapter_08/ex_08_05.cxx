//
// Created by Francesco Fiduccia on 07.07.20.
//

#include "ex_08_05.hxx"
#include "lib/stream_extra.hxx"

namespace ffi::_08::_05 {

void run() {
  try {
    EX_08_05 e;
    cout << endl << "=== 08_05 === " << e.name << endl;
    e.run(-13, 17);
  } catch (std::exception &e) {
    cout << "Error: '" << e.what() << "'" << endl;
  }
}

void EX_08_05::run(const long n, const long m) {
  long result;
  if (n < m) {
    result = this->execute(m, n);
  } else {
    result = this->execute(n, m);
  }
  cout << n << " * " << m << " = " << result << endl;
}

long EX_08_05::execute(const long n, const long m) {

  if (m == 1) {
    return n;
  } else if (m == 0) {
    return 0;
  }

  vector<pair<long, long>> partials{{1, n}};
  auto i = 0;
  while ((partials[i].first + partials[i].first) < m) {
    i++;
    auto prevValue = partials[i - 1].second;
    auto prevMultiplier = partials[i - 1].first;
    partials.emplace_back(prevMultiplier + prevMultiplier, prevValue + prevValue);

  }
  cout << "rounds: " << i + 1 << endl;
  cout << "=>    : " << partials << endl;

  auto j = m;
  auto result = 0;
  while (j != 0) {
    if (j >= partials[i].first) {
      cout << '+' << partials[i].second << " @[" << partials[i].first << ']' << endl;
      result += partials[i].second;
      j -= partials[i].first;
    }
    i--;
  }
  return result;
}

}