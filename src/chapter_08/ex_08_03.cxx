//
// Created by Francesco Fiduccia on 04.07.20.
//

#include "ex_08_03.hxx"
#include <iostream>
#include <iomanip>
#include <array>
#include <lib/dice.hxx>
#include <stack>

namespace ffi::_08::_03 {
void run() {
  try {
    EX_08_03 e;
    cout << endl << "=== 08_03 === " << e.name << endl;
    e.run();
  } catch (std::exception &e) {
    cout << "Error: '" << e.what() << "'" << endl;
  }
}

typedef pair<int32_t, int32_t> range;
ostream &operator<<(ostream &out, const range &r) {
  out << '(' << setfill(' ') << setw(3) << r.first << ',' << setw(3) << r.second << ')';
  return out;
}
template<unsigned WINDOW>
int32_t findMagicalIndex(std::array<int32_t, WINDOW> &data) {
  stack<range> ranges;

  ranges.emplace(0, WINDOW - 1);

  while (!ranges.empty()) {
    range r = ranges.top();

    ranges.pop();
    auto m = r.first + ((r.second - r.first) / 2);
    auto v = data[m];
    cout << setfill(' ') << "m: " << setw(3) << m << " v: " << setw(3) << v << " checking range: " << r << endl;
    if (v == m) {
      return m;
    }

    if (v < m) {
      if (m < r.second)
        ranges.emplace(m + 1, r.second);
      if (v > r.first)
        ranges.emplace(r.first, v);
    } else {
      if (v < r.second)
        ranges.emplace(v, r.second);
      if (m > r.first)
        ranges.emplace(r.first, m - 1);
    }
  }
  return -1;
}

double standard_deviation(const vector<int32_t> &data, const double mean) {

  double acc_sq_diff = accumulate(data.begin(), data.end(), 0, [mean](auto &i, auto &e) {
    auto diff = e - mean;
    return diff * diff + i;
  });
  acc_sq_diff /= data.size() - 1;

  return sqrt(acc_sq_diff);
}

void EX_08_03::run() {
  const static auto WINDOW = 64;

  const static auto POPULATION = 5000;
  const static auto DICE_MIN = -WINDOW / 2;
  const static auto DICE_MAX = WINDOW * 1.5;

  std::vector<int32_t> magicIndexes{};

  for (int j = 0; j < POPULATION; j++) {
    array<int32_t, WINDOW> data{};
    ffi::Dice dice(j);
    for (int i = 0; i < WINDOW; ++i) {
      data[i] = dice.next(DICE_MIN, DICE_MAX);
    }
    sort(data.begin(), data.end());

    cout << "array: ";
    for (auto &e : data) {
      cout << setw(2) << setfill('0') << e << ' ';
    }
    cout << endl;
    auto index = findMagicalIndex<WINDOW>(data);
    if (index > -1 && index < WINDOW) {
      cout << "magic index: " << index << endl;
      magicIndexes.push_back(index);
    } else {
      cout << "the magic index has not been found" << endl;
    }
  }
  cout << setfill('-') << setw(64) << ' ' << endl;
  sort(magicIndexes.begin(), magicIndexes.end());
  cout << "found total of: " << magicIndexes.size() << '/' << POPULATION << " ~=  "
       << magicIndexes.size() / (double) POPULATION << endl;

  cout << "with range: " << make_pair(magicIndexes.front(), magicIndexes.back()) << endl;
  cout << "median: " << magicIndexes[magicIndexes.size() / 2] << endl;
  auto sum = accumulate(magicIndexes.begin(), magicIndexes.end(), 0);
  auto mean = sum / magicIndexes.size();

  cout << "mean: " << mean << endl;
  cout << "standard deviation: " << standard_deviation(magicIndexes, mean);
  cout << endl;
}
}