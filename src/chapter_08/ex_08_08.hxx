//
// Created by Francesco Fiduccia on 07.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_EX_08_08_HXX_
#define CTCI_2020_SRC_CHAPTER_08_EX_08_08_HXX_
#include <string>

#include <unordered_set>
#include <unordered_map>
#include <lib/dice.hxx>
#include <array>
#include <list>
#include <lib/custom_set.hxx>

namespace ffi::_08::_08 {
void run();

class EX_08_08 {

 public:
  string name = "String permutation - With duplicates";
  void run();
  explicit EX_08_08(const string &text, bool printStatus = false);

 private:

  unordered_map<char, unsigned> characterCountMap;
  vector<char> characters;
  vector<string> results;
  int moves;
  bool printStatus;
  string partial;

  unsigned currentCharactersCount = 0;
  unsigned sourceStringLength;

  unsigned maxCharacterIndex;

  void generatePermutation(const unsigned position = 0);

  bool consumeNextCharacter(unsigned &index, char &foundChar);
  void reinsertConsumedChar(char a_char);
  void printStepStatus(unsigned int index, char foundChar) const;
  void pushSolution(const unsigned position);
};
}
#endif //CTCI_2020_SRC_CHAPTER_08_EX_08_08_HXX_
