//
// Created by Francesco Fiduccia on 11.07.20.
//

#include "ex_08_10.hxx"
#include "lib/dice.hxx"
#include "lib/stream_extra.hxx"
#include <lib/console.hxx>
using namespace std;
namespace ffi::_08::_10 {

class ImageWrapper : public ffi::ImageColorWrapper {

 private:
  const Image &image;
  const Point &point;

 public:
  ImageWrapper(const Image &image, const Point &point)
      : image(image),
        point(point) {

  }

  unsigned int height() const override {
    return image[0].size();
  }
  unsigned int width() const override {
    return image.size();
  }
  ffi::ConsoleColor color(unsigned int x, unsigned int y) const override {
    if (x == point.first && y == point.second) {
      return ConsoleColor::black;
    }
    return static_cast<ConsoleColor>(ffi::ConsoleColor::white + image[x][y]);
  }
};

void run() {
  // for (int i = 1; i <20; i+= 3) {
  try {

    EX_08_10 e(20);
    cout << endl << "=== 08_10 === " << e.name << endl;
    e.run({11, 12}, 3);
  } catch (std::exception &e) {
    cout << "Error: '" << e.what() << "'" << endl;
  }
}
const vector<pair<int, int>> EX_08_10::directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

void EX_08_10::run(Point p, Color color) {


  ImageWrapper wrapper(this->image, p);
  ffi::Console::printImage(wrapper);

  frontier.push_back(p);
  originalColor = image[p.first][p.second];
  overrideColor = color;
  while (!frontier.empty()) {
    auto cell = frontier.front();
    frontier.pop_front();
    for (const auto &diff : directions) {
      evaluateCell( cell, diff.first, diff.second);
    }
  }

  cout << endl;
  ffi::Console::printImage(wrapper);
}

EX_08_10::EX_08_10(unsigned size) {
  Dice dice(size);
  image.resize(size);
  for (int i = 0; i < size; ++i) {
    image[i].resize(size);
    for (int j = 0; j < size; ++j) {
      image[i][j] = dice.next(1, 4);
    }
  }
  evaluated.resize(image.size(), vector<bool>(image.size()));

}

void EX_08_10::evaluateCell(const Point &p,
                            const int dx,
                            const int dy) {

  auto x = p.first + dx;
  auto y = p.second + dy;
  if (x > image.size() && y > image.size())
    return;
  if (!evaluated[x][y] && image[x][y] == originalColor) {
    frontier.emplace_back(x, y);
    image[x][y] = overrideColor;
  }
  evaluated[x][y] = true;
}

}