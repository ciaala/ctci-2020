//
// Created by Francesco Fiduccia on 07.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_EX_08_07_HXX_
#define CTCI_2020_SRC_CHAPTER_08_EX_08_07_HXX_
#include <string>
#include <string>
#include <unordered_set>
#include <lib/dice.hxx>
#include <array>
#include <list>
#include <lib/custom_set.hxx>

namespace ffi::_08::_07 {
void run();

class EX_08_07 {

 public:
  string name = "String permutation";
  void run();
  explicit EX_08_07(string text);

 private:

  string elements;
  CustomSet custom_set;
  void generatePermutation(vector<string> &results,
                           string &partial);

  void generatePreviousPermutation(
      unordered_set<char> &characters,
      vector<string> &results,
      string &partial);
};
}
#endif //CTCI_2020_SRC_CHAPTER_08_EX_08_07_HXX_
