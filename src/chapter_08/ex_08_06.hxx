//
// Created by Francesco Fiduccia on 07.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_EX_08_06_HXX_
#define CTCI_2020_SRC_CHAPTER_08_EX_08_06_HXX_
#include <string>
#include <string>
#include <unordered_set>
#include <lib/dice.hxx>
#include <array>
#include <list>

namespace ffi::_08::_06 {
using namespace std;

void run();

class EX_08_06 {
 public:
  string name = "Tower of Hanoi";
  void run(const unsigned height);
  void moveTower(array<vector<unsigned>, 3> &towers,
                 const unsigned disks,
                 const unsigned source,
                 const unsigned target,
                 const unsigned support);
  void printTowers(const array<vector<unsigned>, 3> &towers) const;
};
}

#endif //CTCI_2020_SRC_CHAPTER_08_EX_08_06_HXX_
