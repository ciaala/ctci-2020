//
// Created by Francesco Fiduccia on 07.07.20.
//

#ifndef CTCI_2020_SRC_LIB_STREAM_EXTRA_HXX_
#define CTCI_2020_SRC_LIB_STREAM_EXTRA_HXX_

#include <vector>
#include <unordered_set>
#include <ostream>
#include <iomanip>

namespace ffi {

template<typename T, typename V>
ostream &operator<<(ostream &out, const pair<T, V> &r);

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &flagSelected);

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &flagSelected);

template<typename T>
ostream &operator<<(ostream &out, const unordered_set<T> &set);

template<typename T>
ostream &operator<<(ostream &out, const list<T> &elements);

template<typename T>
ostream &operator<<(ostream &out, const vector<T> &elements) {
  for (int i = 0; i < elements.size() - 1; i++) {
    out << elements[i] << ' ';
  }
  if (!elements.empty()) {
    out << elements.back();
  }
  return out;
}

template<typename T>
ostream &operator<<(ostream &out, const list<T> &elements) {
  for_each(begin(elements),--end(elements), [&out](const T &e) { out << e << ' '; });

  if (!elements.empty()) {
    out << elements.back();
  }
  return out;
}

template<typename T>
ostream &operator<<(ostream &out, const unordered_set<T> &set) {
  vector<T> elements(set.begin(), set.end());
  sort(elements.begin(), elements.end());
  out << elements;
  return out;
}

template<typename T, typename V>
ostream &operator<<(ostream &out, const pair<T, V> &r) {
  out << '(' << r.first << ',' << r.second << ')';
  return out;
}

}

#endif //CTCI_2020_SRC_LIB_STREAM_EXTRA_HXX_
