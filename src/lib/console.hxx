//
// Created by Francesco Fiduccia on 03.07.20.
//

#ifndef CTCI_2020_SRC_LIB_CONSOLE_HXX_
#define CTCI_2020_SRC_LIB_CONSOLE_HXX_

#include <string>
namespace ffi {

enum ConsoleColor {
  white = 0,
  red = 1,
  green = 2,
  yellow = 3,
  blue = 4,
  magenta = 5,
  cyan = 6,
  gray = 7,
  dark_gray = 8,
  bright_red = 9,
  bright_green = 10,
  bright_yellow = 11,
  bright_blue = 12,
  bright_magenta = 13,
  bright_cyan = 14,
  black = 15
};

class ImageColorWrapper {
 public:
  virtual unsigned height() const = 0;
  virtual unsigned width() const = 0;
  virtual ConsoleColor color(unsigned x, unsigned y) const = 0;
};

class Console {
 private:

  static std::string escapeBackground(ConsoleColor color);
 public:
  static bool clear();
  static void printImage(const ImageColorWrapper &image);
};

}
#endif //CTCI_2020_SRC_LIB_CONSOLE_HXX_
