//
// Created by Francesco Fiduccia on 28.06.20.
//

#ifndef GOOGLE_2020_SRC_LIB_BINARYTREE_HXX_
#define GOOGLE_2020_SRC_LIB_BINARYTREE_HXX_
namespace ffi {

template<typename T>
class BinaryTreeNode {

 private:
  const T _value;
  BinaryTreeNode<T> *_left;
  BinaryTreeNode<T> *_right;

 public:
  explicit BinaryTreeNode<T>(const T value) : _value(value) {
  }

  void set_left(BinaryTreeNode<T> *node) {
    this->_left = node;
  }

  const T &value() const {
    return this->_value;
  }

  void set_right(BinaryTreeNode<T> *node) {
    this->_right = node;
  }

  BinaryTreeNode<T> *create_left(const T &value) {
    this->_left = new BinaryTreeNode(value);
    return this;
  }

  BinaryTreeNode<T> *create_right(const T &value) {
    this->_right = new BinaryTreeNode(value);
    return this;
  }

  BinaryTreeNode<T> *left() { return this->_left; }

  BinaryTreeNode<T> *right() { return this->_right; }

};

}
#endif //GOOGLE_2020_SRC_LIB_BINARYTREE_HXX_
