//
// Created by Francesco Fiduccia on 03.07.20.
//

#include "console.hxx"

#include <term.h>
#include <unistd.h>
#include <sstream>
#include <iostream>

bool ffi::Console::clear() {

  static int result;
  static const int setup = setupterm(NULL, STDOUT_FILENO, &result);
  static char clearString[] = "clear";
  static const char *clear = tigetstr(clearString);
  putp(clear);
  return true;
}
void ffi::Console::printImage(const ImageColorWrapper &image) {
  for (int i = 0; i < image.width(); ++i) {
    for (int j = 0; j < image.height(); ++j) {
      auto color = image.color(i, j);
      auto sequence = escapeBackground(color);
      std::cout << "\033[" << sequence << "m \033[m";
    }
    std::cout << std::endl;
  }
}

std::string ffi::Console::escapeBackground(ffi::ConsoleColor color) {
  std::stringstream ss;
  auto offset = color < ConsoleColor::dark_gray ? 40 : (100 - 8);
  ss << (offset + color);
  return ss.str();
}


