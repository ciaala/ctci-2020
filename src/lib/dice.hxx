//
// Created by Francesco Fiduccia on 04.07.20.
//

#ifndef CTCI_2020_SRC_LIB_DICE_HXX_
#define CTCI_2020_SRC_LIB_DICE_HXX_

#include <random>
#include <iostream>

using namespace std;
namespace ffi {
class Dice {
  std::default_random_engine generator;
  unsigned int seed;
 public:
  Dice() : Dice((unsigned int) clock()) {

  }

  explicit Dice(unsigned int seed) : seed(seed), generator(seed) {
    cout << "Seed: " << seed << endl;
  }

  uint32_t next(uint32_t base, uint32_t maximum) {
    std::uniform_int_distribution<uint32_t> distribution(base, maximum);
    return distribution(generator);
  }
};
}
#endif //CTCI_2020_SRC_LIB_DICE_HXX_
