//
// Created by Francesco Fiduccia on 07.07.20.
//

#ifndef CTCI_2020_SRC_LIB_BENCHMARK_HXX_
#define CTCI_2020_SRC_LIB_BENCHMARK_HXX_

#include <chrono>
#include <iostream>
#include <string>

namespace ffi {

class TimeBenchmark {
 private:
  std::chrono::time_point<std::chrono::system_clock> _start;
  std::chrono::time_point<std::chrono::system_clock> _finish;
  bool _started;
  bool _valid;
  std::string name;

 public:
  TimeBenchmark(std::string name) : _started(false), _valid(false) {
    this->name = name;
  }

  void start() {
    if (_started == false) {
      this->_start = std::chrono::system_clock::now();
      this->_started = true;
    }
  }
  void finish() {
    if (_started) {
      this->_finish = std::chrono::system_clock::now();
      this->_valid = true;
    }
  }

  auto getDuration(const TimeBenchmark &time_benchmark) const {
    auto fd = time_benchmark._finish - time_benchmark._start;
    return std::chrono::duration_cast<std::chrono::milliseconds>(fd).count();
  }

  friend std::ostream &operator<<(std::ostream &out, const TimeBenchmark &time_benchmark);
};

std::ostream &operator<<(std::ostream &out, const TimeBenchmark &time_benchmark);

}
#endif //CTCI_2020_SRC_LIB_BENCHMARK_HXX_
