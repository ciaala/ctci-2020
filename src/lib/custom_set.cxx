//
// Created by Francesco Fiduccia on 08.07.20.
//

#include "custom_set.hxx"
namespace ffi {
const unsigned CustomSet::BINARY_GRANULARITY = 4;
int CustomSet::findLocationFirstTrue(unsigned long firstIndex) {
  if (_count_trues == 0) {
    return -1;
  }

  for (unsigned i = firstIndex / BINARY_GRANULARITY; i < binarySpace.size(); ++i) {
    if (binarySpace[i] > 0) {
      unsigned j = i * BINARY_GRANULARITY < firstIndex ? firstIndex : i * BINARY_GRANULARITY;
      for (; j < (i + 1) * BINARY_GRANULARITY; ++j) {
        if (selectionSet[j])
          return j;
      }
    }
  }
  return -1;
}

void CustomSet::set(unsigned int location, bool value) {
  if (selectionSet[location] != value) {
    binarySpace[location / BINARY_GRANULARITY] += value ? 1 : -1;
    selectionSet[location] = value;
    if (value)
      _count_trues += 1;
    else
      _count_trues -= 1;
  }
}
void CustomSet::init_partitioning_space(unsigned int size, bool defaultValue) {
  unsigned partitionSize = size / BINARY_GRANULARITY;
  unsigned highBits = size % BINARY_GRANULARITY;
  if (highBits) {
    partitionSize++;
  }
  binarySpace.resize(partitionSize, defaultValue ? BINARY_GRANULARITY : 0);
  if (highBits) {
    binarySpace[partitionSize - 1] = highBits;
  }
}
CustomSet::CustomSet(unsigned int size, bool defaultValue) {
  selectionSet.resize(size, defaultValue);
  this->init_partitioning_space(size, defaultValue);
  _count_trues = defaultValue ? size : 0;
}
bool CustomSet::get(unsigned int location) {
  return selectionSet[location];
}
unsigned CustomSet::countTrue() {
  return _count_trues;
}
}