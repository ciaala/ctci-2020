//
// Created by Francesco Fiduccia on 03.07.20.
//

#ifndef CTCI_2020_SRC_LIB_GRID_2_D_HXX_
#define CTCI_2020_SRC_LIB_GRID_2_D_HXX_

#include <array>
#include <ostream>
#include <unordered_map>
#include <functional>

using namespace std;

namespace ffi {

struct hash_pair {
  template<class T1, class T2>
  size_t operator()(const pair<T1, T2> &p) const {
    auto hash1 = hash<T1>{}(p.first);
    auto hash2 = hash<T2>{}(p.second);
    return hash1 ^ hash2;
  }
};

template<typename GridCell, unsigned int sizeX, unsigned int sizeY = sizeX>
class Grid2D {
 private:
  array<array<GridCell, sizeX>, sizeY> grid;
 public:
  explicit Grid2D(const GridCell initialValue) {
    std::array<GridCell, sizeX> initialValueRow{initialValue};
    initialValueRow.fill(GridCell::Empty);
    grid.fill(initialValueRow);
  }

  void print(const std::function<GridCell(unsigned, unsigned, GridCell)> &overlay,
             ostream &out) const {
    for (int j = 0; j < grid.size() + 2; ++j) {
      out << '-';
    }
    out << endl;

    for (int i = 0; i < grid.size(); ++i) {
      const auto &row = grid[i];
      out << '|';
      for (int j = 0; j < row.size(); ++j) {
        const auto &cell = overlay(i, j, row[j]);
        out << (char) cell;
      }
      out << '|' << endl;

    }
    for (int j = 0; j < grid.size() + 2; ++j) {
      out << '-';
    }
    out << endl;
  }

  GridCell get(unsigned int i, unsigned int j) const {
    return grid[i][j];
  }
  void set(unsigned int i, unsigned int j, GridCell cell) {
    grid[i][j] = cell;
  }
};

}

#endif //CTCI_2020_SRC_LIB_GRID_2_D_HXX_
