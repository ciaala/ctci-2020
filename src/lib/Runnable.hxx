//
// Created by Francesco Fiduccia on 01.07.20.
//

#ifndef GOOGLE_2020_SRC_LIB_RUNNABLE_HXX_
#define GOOGLE_2020_SRC_LIB_RUNNABLE_HXX_

class Runnable {
 public:
  virtual void run() = 0;
};
#endif //GOOGLE_2020_SRC_LIB_RUNNABLE_HXX_
