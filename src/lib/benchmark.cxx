//
// Created by Francesco Fiduccia on 09.07.20.
//
#include <ostream>
#include "benchmark.hxx"
using namespace std;

ostream &ffi::operator<<(ostream &out, const TimeBenchmark &time_benchmark) {
  if (time_benchmark._valid) {
    long long int time = time_benchmark.getDuration(time_benchmark);
    out << "'" << time_benchmark.name << "' execution time: " << time << "ms" << std::endl;
  } else {
    out << "TimerBenchmark::finish() had not been called" << endl;
  }
  return out;
}