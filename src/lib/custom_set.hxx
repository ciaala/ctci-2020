//
// Created by Francesco Fiduccia on 08.07.20.
//

#ifndef CTCI_2020_SRC_CHAPTER_08_CUSTOM_SET_HXX_
#define CTCI_2020_SRC_CHAPTER_08_CUSTOM_SET_HXX_
#include <vector>
namespace ffi {
using namespace std;

class CustomSet {
 private:
  vector<int8_t> binarySpace;
  vector<bool> selectionSet;
  unsigned _count_trues;

 public:
  static const unsigned BINARY_GRANULARITY;

  explicit CustomSet(unsigned int size, bool defaultValue);

  bool get(unsigned location);

  void set(unsigned location, bool value);

  int findLocationFirstTrue(unsigned long firstIndex = 0);

  unsigned countTrue();

//
//
//
 private:
  void init_partitioning_space(unsigned int size, bool defaultValue);
};
}

#endif //CTCI_2020_SRC_CHAPTER_08_CUSTOM_SET_HXX_
