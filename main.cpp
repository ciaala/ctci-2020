#include <iostream>
#include "./all_chapters.hxx"

using namespace std;
int main() {
  cout << "Hello, World!" << endl;
  cout << endl << "- Chapter  4 --------------" << endl;

  ffi::_04::run();
  cout << endl << "- Chapter  8 --------------" << endl;

  ffi::_08::run();
  return 0;
}
